package com.viettel.listener;

import com.viettel.domain.VtSongImport;
import com.viettel.domain.VtSongRepository;
import com.viettel.queue.VtSongImportQueue;
import com.viettel.utils.StringUtils;
import java.io.BufferedOutputStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

/**
 * Created by dangduc on 20/6/2019.
 */
public class VtSongImportQueueListener extends Thread {

    private static final Logger logger = Logger.getLogger(VtSongImportQueueListener.class);
    private static final Logger loggerTransaction = Logger.getLogger("TRANSACTION");
    private final String threadName;
    private FTPClient ftpClient;

    public VtSongImportQueueListener(int index) {
        this.threadName = "Listener " + index;
    }

    public String convert2noSign(String source) {
        try {
            String sign = "ữựừứửưũụùúủễệềếểêẽẹèéẻỹỵỳýỷĩịìíỉỡợờớởơỗộồốổôõọòóỏẵặằắẳăẫậầấẩâãạàáảđỮỰỪỨỬƯŨỤÙÚỦỄỆỀẾỂÊẼẸÈÉẺỸỴỲÝỶĨỊÌÍỈỠỢỜỚỞƠỖỘỒỐỔÔÕỌÒÓỎẴẶẰẮẲĂẪẬẦẤẨÂÃẠÀÁẢĐ ";
            String nosign = "uuuuuuuuuuueeeeeeeeeeeyyyyyiiiiioooooooooooooooooaaaaaaaaaaaaaaaaadUUUUUUUUUUUEEEEEEEEEEEYYYYYIIIIIOOOOOOOOOOOOOOOOOAAAAAAAAAAAAAAAAAD_";
            for (int i = 0; i < sign.length(); i++) {
                source = source.replaceAll(sign.charAt(i) + "", nosign.charAt(i) + "");
            }
        } catch (Exception e) {
        }
        return source;
    }

    @Override
    public void run() {
    
        while (!Thread.currentThread().isInterrupted()) {

            try {
                VtSongImport songImport = VtSongImportQueue.getInstance().poll();
                if (songImport != null) {
                    logger.info(songImport.toString());
                    downloadFromFtp(songImport);
                    VtSongRepository repository = new VtSongRepository();
                    //B2.2
                    //edit path
                    //edit by dupd
                    if (org.springframework.util.StringUtils.isEmpty(songImport.getPath())) {
                        boolean updateImprot = repository.updateImportDetailStatus(songImport.getId(), 2);
                        loggerTransaction.info(songImport + "|" + "|" + updateImprot + "|PATH IS NULL OR BLANK");
                    } else {
                        try {
                            songImport.setPath(songImport.getPath().replace("\r", "").replace("\n", "").trim());
                            logger.info("Path song : " + songImport.getPath());
                            File file = new File(songImport.getPath());
                            File file4G = new File(songImport.getPath_4g());
                            String fileName = file.getName();
                            String fileName4G = file4G.getName();
                            if (fileName != null || fileName.contains(" ")) {
                                String fileNameNew = convert2noSign(fileName);
                                String fileNameNew4G = convert2noSign(fileName4G);
                                String pathFileNew = file.getParent() + "/" + fileNameNew;
                                pathFileNew = pathFileNew.replace("//", "/");
                                String pathFileNew4G = file4G.getParent() + "/" + fileNameNew4G;
                                pathFileNew4G = pathFileNew4G.replace("//", "/");
                                logger.info("Path song stand : " + fileNameNew + " - path : " + pathFileNew + "\n" + songImport.getPath() + " -- " + songImport.getPath_4g());
                                File fileNew = new File(pathFileNew);
                                if (!fileNew.exists()) {
                                    if (file.renameTo(fileNew)) {
                                        logger.info("Rename file success : " + fileNameNew);
                                    } else {
                                        logger.info("Rename file fail : " + fileNameNew);
                                    }
                                }
                                File fileNew4G = new File(pathFileNew4G);
                                if (!fileNew4G.exists()) {
                                    if (file4G.renameTo(fileNew4G)) {
                                        logger.info("Rename file success : " + fileNameNew4G);
                                    } else {
                                        logger.info("Rename file fail : " + fileNameNew4G);
                                    }
                                }
                                file = new File(pathFileNew);
                                songImport.setPath(pathFileNew);
                                songImport.setPath_4g(pathFileNew4G);
                            }
                            if (!file.exists() || songImport.getSongName().trim().equals("")) {
                                boolean updateImprot = repository.updateImportDetailStatus(songImport.getId(), 2);
                                loggerTransaction.info(songImport + "|" + "|" + updateImprot + "FILE_NOT_FOUND OR NAME_BLANK");
                            } else {
                                //B2.3
                                String[] singerNames = songImport.getSingerNameUnsign().split("ft");
                                boolean isValidSingers = true;
                                List<Long> singerIds = new ArrayList<>();
                                for (String singerName : singerNames) {
                                    try {
                                        singerName = singerName.trim();
                                    } catch (Exception ex) {
                                        logger.info("Trim singer name : " + singerName + " | " + ex.toString());
                                    }
                                    Long singerId = 0L;
                                    if (singerName != null && !"".equals(singerName)) {
                                        singerId = repository.getSingerId(singerName);
                                    }
                                    if (singerId <= 0) {
                                        isValidSingers = false;
                                        break;
                                    }
                                    singerIds.add(singerId);
                                }
                                if (!isValidSingers) {
                                    boolean updateImport = repository.updateImportDetailStatus(songImport.getId(), 5);
                                    loggerTransaction.info(songImport + "|" + updateImport + "|SINGER_NOT_EXIST");
                                } else {
                                    if (isSongAndSingersExist(singerIds, songImport.getSongNameUnsign())) {
                                        boolean updateImport = repository.updateImportDetailStatus(songImport.getId(), 4);
                                        loggerTransaction.info(songImport + "|" + "|" + updateImport + "|SONG_EXIST");
                                    } else {
                                        //B2.3.1
                                        Long songId = repository.insertSong(songImport);
                                        //B2.3.2
                                        Long resultId = repository.insertImportResult(songId, songImport.getId());
                                        logger.debug("import result|" + resultId);

                                        //B2.3.3
                                        for (String singerName : singerNames) {
                                            try {
                                                singerName = singerName.trim();
                                            } catch (Exception ex) {
                                                logger.info("Trim singer name : " + singerName + " | " + ex.toString());
                                            }
                                            Long singerId = 0L;
                                            if (singerName != null && !"".equals(singerName)) {
                                                singerId = repository.getSingerId(singerName);
                                            }
                                            if (singerId > 0) {
                                                repository.mapSongAndSinger(songId, singerId);
                                            }
                                        }

                                        //B2.3.4
                                        String[] authorNames = songImport.getAuthorNameUnsign().split("ft");
                                        for (String authorName : authorNames) {
                                            try {
                                                authorName = authorName.trim();
                                            } catch (Exception ex) {
                                                logger.info("Trim author name : " + authorName + " | " + ex.toString());
                                            }
                                            Long authorId = 0L;
                                            if (authorName != null && !"".equals(authorName)) {
                                                authorId = repository.getAuthorId(authorName);
                                            }
                                            if (authorId <= 0) {
                                                authorId = repository.insertAuthor(songImport.getAuthorName(), songImport.getAuthorNameUnsign());
                                            }
                                            if (authorId > 0) {
                                                repository.mapSongAndAuthor(songId, authorId);
                                            }
                                        }

                                        //B2.3.4.1
                                        Long categoryId = repository.getCategoryId(songImport.getCategoryNameUnsign());
                                        if (categoryId > 0) {
                                            repository.mapSongAndCategory(songId, categoryId);
                                        }

                                        //B2.3.5
                                        Long convertWapId = repository.insertSongToConvert(songId, songImport.getSongName(), songImport.getPath(), "wap_path");

                                        //B2.3.6
                                        Long convertWapMonoId = repository.insertSongToConvert(songId, songImport.getSongName(), songImport.getPath(), "wap_path_mono");

                                        //B2.3.7
                                        boolean updateDetailImprot = repository.updateImportDetailStatus(songImport.getId(), 3);

                                        loggerTransaction.info(songImport + "|" + songId + "|" + resultId + "|" + categoryId + "|" + convertWapId + "|" + convertWapMonoId + "|" + updateDetailImprot);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            logger.error(e.toString());
                        }
                    }

                    //B3
                    boolean updateDetailImprot = repository.updateImportStatus(songImport.getImportId(), 3);
                    loggerTransaction.debug("done|" + updateDetailImprot);
                } else {
                    logger.debug(threadName + "|is running");
                    Thread.sleep(1000L);
                }
                Thread.sleep(1000L);

            } catch (Exception ex) {
                logger.error(StringUtils.getStackTrace(ex));
                Thread.currentThread().interrupt();
            }
        }
    }

    private void downloadFromFtp(VtSongImport songImport) {
        if (songImport == null) {
            return;
        }
        logger.info("Check data downloadFromFtp : " + songImport.toString());
        try {
            if (songImport.getPath() != null && "".equals(songImport.getPath()) == false) {
                File file = new File(songImport.getPath());
                if (!file.exists()) {
                    logger.info("downloadFromFtp : " + file.getPath());
                    download(file.getPath(), file.getPath());
                }
            }
            if (songImport.getPath_4g() != null && "".equals(songImport.getPath_4g()) == false) {
                File file4G = new File(songImport.getPath_4g());
                if (!file4G.exists()) {
                    logger.info("downloadFromFtp : " + file4G.getPath());
                    download(file4G.getPath(), file4G.getPath());
                }
            }
        } catch (Exception e) {
            logger.error("downloadFromFtp : " + e.toString());
        }
    }

    public boolean download(String pathSrc, String pathDes) {
        OutputStream oStream;
        try {
            if (ftpClient == null) {
                ftpClient = new FTPClient();
            } else {
                logger.info("FTPClient is not null");
            }
            ftpClient.connect("10.30.142.11", 21);
            ftpClient.login("ftp_user", "Media@2017");
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            File file = new File(pathDes);
            File folder = new File(file.getParent());
            if (!folder.exists()) {
                if (folder.mkdirs()) {
                    logger.info("Create directory is success");
                }
            }
            boolean success;
            oStream = new BufferedOutputStream(new FileOutputStream(pathSrc));
            try {
                success = ftpClient.retrieveFile(pathDes, oStream);
                if (success) {
                    logger.info("downloadFromFtp Successfully: " + pathSrc);
                } else {
                    logger.info("downloadFromFtp Fail: " + pathSrc);
                }
                ftpClient.logout();
            } catch (Exception e) {
            } finally {
                try {
                    if (oStream != null) {
                        oStream.close();
                    }
                    ftpClient.disconnect();
                } catch (IOException e) {
                    logger.error(e.toString());
                }
            }

        } catch (Exception ex) {
            logger.error("download : " + ex.toString());
        }
        return true;
    }

    private boolean isSongAndSingersExist(List<Long> singerIds, String songNameUnsign) {
        boolean result = false;
        VtSongRepository repository = new VtSongRepository();
        List<Long> songIds = repository.getSongIds(songNameUnsign);
        for (Long songId : songIds) {
            List<Long> checkSingerIds = repository.getSingerIds(songId);
            if (checkSingerIds.size() != singerIds.size()) {
                continue;
            } else {
                if (checkSingerIds.containsAll(singerIds)) {
                    return true;
                } else {
                    continue;
                }
            }
        }
        return result;
    }
}
